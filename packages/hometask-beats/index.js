import Swiper, { Navigation, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

const swiperMobile = new Swiper(".swiper", {
    modules: [Navigation, Pagination],
    navigation: {
      nextEl: ".slide-next",
      prevEl: ".slide-prev",
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
      },
    },
    slidesPerView: 1,
    centeredSlides: true,
    centeredSlidesBounds: true,
  });

  const menuSection = document.querySelector('.menu__section');

  const menu = document.getElementById("menu");
  menu.addEventListener('click', () => {
    menuSection.style.display = "block";
  })


  const textMenu = document.getElementById("menu__text");
  textMenu.addEventListener('click', () => {
    menuSection.style.display = "none";
  })