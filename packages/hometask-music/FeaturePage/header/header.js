import { createElement } from "../../components/createElement";
import "./header.css";
import logo from "../../assets/icons/logo.svg";

export default function startFeatHeader(parent) {
    createElement({ tag:'div', attributes: {class: 'root__header root__header-feat'}, parent});
    const header = document.querySelector(".root__header");

    createElement({tag: 'img', attributes: {class: 'header__logo', src: logo}, parent: header,});
    createElement({tag: 'p', attributes: {class: 'header__text'}, parent: header, content: 'Simo'});
    createElement({tag: 'p', attributes: {class: 'header__text', id: 'join-feat'}, parent: header, content: 'Join'});
    createElement({tag: 'p', attributes: {class: 'header__text'}, parent: header, content: 'Sign in'});
}