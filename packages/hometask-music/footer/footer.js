import { createElement } from "../components/createElement";
import "./footer.css";

export default function startFooter(parent) {
    createElement({tag: 'footer', attributes: {class: 'root__footer'}, parent});
    const footer = document.querySelector('.root__footer');

    createElement({tag: 'p', attributes: {class: 'footer__text'}, parent: footer, content: 'About us'});
    createElement({tag: 'p', attributes: {class: 'footer__text'}, parent: footer, content: 'Contact'});
    createElement({tag: 'p', attributes: {class: 'footer__text'}, parent: footer, content: 'CR Info'});
    createElement({tag: 'p', attributes: {class: 'footer__text'}, parent: footer, content: 'Twitter'});
    createElement({tag: 'p', attributes: {class: 'footer__text'}, parent: footer, content: 'Facebook'});
}