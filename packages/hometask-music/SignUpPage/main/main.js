import { createElement } from "../../components/createElement";
import "./main.css";
import signUpGirl from '../../assets/images/background-page-sign-up.png';

export default function startSignUpMain(parent) {
    createElement({tag: 'div', attributes: {class: 'root__main root__main-signUpPage'}, parent});
    const main = document.querySelector(".root__main");

    createElement({tag: 'div', attributes: {class: 'main__signin__inputs'}, parent: main});
    const inputs = document.querySelector('.main__signin__inputs');

    createElement({tag: 'div', attributes: {class: 'main__input__name'}, parent: inputs});
    const inputName = document.querySelector('.main__input__name');

    createElement({tag: 'p', attributes: {class: 'main__inputone__text'}, parent: inputName, content: 'Name:'});
    createElement({tag: 'input', attributes: {class: 'main__input', type: 'text'}, parent: inputName});

    createElement({tag: 'div', attributes: {class: 'main__input__password'}, parent: inputs});
    const inputPassword = document.querySelector('.main__input__password');

    createElement({tag: 'p', attributes: {class: 'main__inputone__text'}, parent: inputPassword, content: 'Password:'});
    createElement({tag: 'input', attributes: {class: 'main__input', type: 'text'}, parent: inputPassword});

    createElement({tag: 'div', attributes: {class: 'main__input__email'}, parent: inputs});
    const inputEmail = document.querySelector('.main__input__email');

    createElement({tag: 'p', attributes: {class: 'main__inputone__text'}, parent: inputEmail, content: 'e-mail:'});
    createElement({tag: 'input', attributes: {class: 'main__input', type: 'text'}, parent: inputEmail});

    createElement({tag: 'button', attributes: {class: 'main__button-JoinNow', id: 'button-JoinNow'}, parent: main, content: 'Join now'});

    createElement({tag: 'img', attributes: {class: 'main__image__girl main__image__girl-signUpPage', src: signUpGirl}, parent});
}